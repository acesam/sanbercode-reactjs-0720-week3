import React from 'react';

class Welcome extends React.Component {
  render() {
    return <p>{this.props.name}</p>;
  }
}

class ShowAge extends React.Component {
  render() {
    return <p>{this.props.harga}</p>;
  }
}

class Berat extends React.Component {
  render() {
    return <p> {this.props.berat} kg </p>
  }
}


var buah = [
  {name: "Semangka", harga: 10000, berat: 1000},
  {name: "Anggur", harga: 40000, berat: 500},
  {name: "Strawberry", harga: 30000, berat: 400},
  {name: "Jeruk", harga: 30000, berat: 1000},
  {name: "Mangga", harga: 30000, berat: 500}
]

class HargaBuah extends React.Component {
  render() {
    return (
      <>
        {buah.map(el=> {
          return (

            <div>
            <table>
                <tr>
                  <th>Company</th>
                  <th>Contact</th>
                  <th>Country</th>
                </tr>
                <tr>
                  <td><Welcome name={el.name}/></td>
                  <td><ShowAge harga={el.harga}/></td>
                  <td><Berat berat={el.berat}/> </td>
                </tr>
              </table>
            </div>

          )
        })}
      </>
    )
  }
}

export default HargaBuah
