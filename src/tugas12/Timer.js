import React, {Component} from 'react'

class Timer extends Component{
  constructor(props){
    super(props)
    this.state = {
      time: 0,
      date: new Date() 
    }
  }

  componentDidMount(){
    if (this.props.start !== undefined){
      this.setState({time: this.props.start})
    }
    this.timerID = setInterval(
      () => this.tick(),
      10000
    );
  }

  componentDidUpdate(){
  }

  componentWillUnmount(){
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      time: this.state.time + 1 
    });
  }


  render(){
    return(
      <>
      {
        this.state.time >= 0 && (
        <>
        <h1 style={{float: "left"}}>
            Sekarang - jam {this.state.date.toLocaleTimeString()},
        </h1>
        <h1 style={{float: "right"}}>
            Hitung Mundur:  {this.state.time}
        </h1>
        </>
        )
    }
    </>
    )
  }
}

export default Timer
