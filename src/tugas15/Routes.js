import React from "react";
import { Switch, Route } from "react-router-dom";
import Tugas11 from '../tugas11/DaftarBuah';
import Tugas12 from '../tugas12/Timer';
import Tugas13 from '../tugas13/DaftarBuah';
import Tugas14 from '../tugas14/DaftarBuah';
import Tugas15 from '../tugas15/DaftarBuah';
import Nav from '../tugas15/Nav';
import { ThemeProvider } from "./ThemeContext";

export default function App() {
  return (
      <>
        <ThemeProvider>          
          <Nav/>
          <Switch>
            <Route exact path="/">
              <Tugas11 />
            </Route>

            <Route path="/tugas12">
              <Tugas12 start={200} />
            </Route>
            
            <Route path="/tugas13">
              <Tugas13 />
            </Route>

            <Route path="/tugas14">
              <Tugas14 />
            </Route>

            <Route path="/tugas15">
              <Tugas15 />
            </Route>
          
          </Switch>
        </ThemeProvider>
      </>
  );
}
